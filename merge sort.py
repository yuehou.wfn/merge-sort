def merge_sort(li):
    if len(li)<=1:
        return li
     
    left=merge_sort(li[:len(li)//2])
    right=merge_sort(li[len(li)//2:])
   
    return reduce(left,right)

def reduce(left, right):
    result=[]
    left_index=0
    right_index=0
    while True:
        if left[left_index]<right[right_index]:
            result.append(left[left_index])
            left_index += 1
        else:
            result.append(right[right_index])
            right_index += 1
            
        if left_index == len(left):
            result.extend(right[right_index:])
            break
        if right_index == len(right):
            result.extend(left[left_index:])
            break
    return result