import time
import merge_sort

length=[200,400,800,1600,3200,6400]
time_best=[]
for n in length:
    start=time.time()
    merge_sort.merge_sort(list(range(n)))
    end=time.time()
    print(n)
    print(end-start)
    time_best.append(end-start)
print(time_best)

import random
length=[200,400,800,1600,3200,6400]
time_avg=[]                                 
for n in length:
    avg=list(range(n))
    random.shuffle(avg)
    start=time.time()
    merge_sort.merge_sort(avg)
    end=time.time()
    print(n)
    print(end-start)
    time_avg.append(end-start)
print(time_avg)

import matplotlib.pyplot as plt
plt.plot(length,time_best,label='best')
plt.plot(length,time_best2,label='best2')
plt.plot(length,time_avg,label='avg/worst')
plt.title('time complexity of merge sort')
plt.legend()
plt.show()